'use strict'

require('dotenv').config()

const cheerio = require('cheerio')
const { GetSync } = require('../lib/RequestSync')
const _XSRF = require('../utils/xsrf')._xsrfFromCookie

async function UpdateTask(task_id, _changes, _cookie) {
  let endpoint = process.env.AWS_URL + '/task/' + task_id
  let opts = {
    url: endpoint,
    headers: { 'cookie': _cookie }
  }
  let { httpResponse, body } = await GetSync(opts)
  let cookie = httpResponse.headers['set-cookie'].join('; ')
  let _xsrf = _XSRF(cookie)

  let $ = cheerio.load(body)
  let serializeForm = $('form[enctype="multipart/form-data"]').serializeArray()

  let formData = {}
  for (let pair of serializeForm) {
    formData[pair.name] = pair.value
  }
}

UpdateTask(11, {}, 'awslogin="e0yf2A5cZ5BWVzjoeZO9D1aUz4w=?id=MQ==&ip=IjEyNy4wLjAuMSI=&timestamp=MTU1MjEwNzE3MS41NTc5NDU="; HttpOnly; Path=/')