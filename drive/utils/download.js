'use strict'

const fs = require('fs')
const path = require('path')
const GetToken = require('../token')
const request = require('request')

function StreamSync(opts, filePath) {
  return new Promise(function(resolve) {
    let stream = request.get(opts).pipe(fs.createWriteStream(filePath))
    stream.on('finish', () => resolve())
  })
}

async function Download(fileID, filePath) {
  const endpoint = 'https://www.googleapis.com/drive/v3/files/' + fileID + '?alt=media'
  const access_token = await GetToken()
  const AuthorizationHeader = {
    headers: {
      'Authorization': 'Bearer ' + access_token
    }
  }
  let opts = {
    url: endpoint,
    ...AuthorizationHeader
  }

  await StreamSync(opts, filePath)

  return true
}

module.exports = Download