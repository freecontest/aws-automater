'use strict'

const GetToken = require('../token')
const { GetSync } = require('../../lib/RequestSync')

async function Info(fileID) {
  const endpoint = 'https://www.googleapis.com/drive/v3/files/' + fileID
  const access_token = await GetToken()
  const AuthorizationHeader = {
    headers: {
      'Authorization': 'Bearer ' + access_token
    }
  }
  let opts = {
    url: endpoint,
    ...AuthorizationHeader
  }
  let { httpResponse, body } = await GetSync(opts)
  return JSON.parse(body)
}

module.exports = Info