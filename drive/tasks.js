'use strict'

const GetToken = require('./token')
const { GetSync } = require('../lib/RequestSync')

const FileInfo = require('./utils/info')
const FileDownload = require('./utils/download')

async function GetTasks(folderID) {
  const endpoint = 'https://www.googleapis.com/drive/v2/files/' + folderID + '/children'
  const access_token = await GetToken()
  const AuthorizationHeader = {
    headers: {
      'Authorization': 'Bearer ' + access_token
    }
  }
  let opts = {
    url: endpoint,
    ...AuthorizationHeader
  }
  
  let { httpResponse, body } = await GetSync(opts)
  body = JSON.parse(body)

  let tasks = []
  for (let task of body.items) {
    tasks.push(await FileInfo(task.id))
  }

  return tasks
}

module.exports = GetTasks