'use strict'

require('dotenv').config()

const fs = require('fs')
const path = require('path')

const { PostSync } = require('../lib/RequestSync')

async function GetToken() {
  let { client_id, client_secret, refresh_token, token_uri } = process.env
  let opts = {
    url: token_uri,
    formData: {
      client_id,
      client_secret,
      refresh_token,
      'grant_type': 'refresh_token'
    },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  }

  let { httpResponse, body } = await PostSync(opts)
  let { access_token } = JSON.parse(body)

  return access_token
}

module.exports = GetToken