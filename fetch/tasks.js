'use strict'

require('dotenv').config()

const cheerio = require('cheerio')
const { GetSync } = require('../lib/RequestSync')

async function getTasks(cookie) {
  let endpoint = process.env.AWS_URL + '/tasks'
  let opts = {
    url: endpoint,
    headers: { 'cookie': cookie }
  }

  let { httpResponse, body } = await GetSync(opts)
  let $ = cheerio.load(body)

  let tasks = []
  $('tr').each(function(_) {
    if (_ === 0) return ;
    let task = {}
    $(this).children('td').each(function(i) {
      if (i === 0) task.id = $(this).children('input[name="task_id"]').attr('value')
      if (i === 1) task.href = $(this).children('a').attr('href')
      if (i === 2) task.name = $(this).text().trim()
    })
    tasks.push(task)
  })

  return tasks.sort((a, b) => a.id - b.id)
}

module.exports = getTasks