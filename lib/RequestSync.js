'use strict'

const request = require('request')

function RequestSync(isGet, opts) {
  return new Promise(function(resolve, reject) {
      if (isGet) {
         request.get(opts, function (err, httpResponse, body) {
            if (err) return reject({ err, httpResponse })
            resolve({ httpResponse, body })
         })   
      }
      else {
         request.post(opts, function (err, httpResponse, body) {
            if (err) return reject({ err, httpResponse })
            resolve({ httpResponse, body })
         })
      }
	})
}

function GetSync(opts) {
   return RequestSync(1, opts)
}

function PostSync(opts) {
   return RequestSync(0, opts)
}

module.exports = { GetSync, PostSync }