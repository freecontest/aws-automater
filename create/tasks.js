'use strict'

require('dotenv').config()

const { PostSync } = require('../lib/RequestSync')
const _XSRF = require('../utils/xsrf')._xsrf

async function createTask(name, _cookie) {
  let endpoint = process.env.AWS_URL + '/tasks/add'
  let opts = {
    url: endpoint,
    headers: { 'cookie': _cookie }
  }

  let { cookie, _xsrf } = await _XSRF(opts)

  opts.headers['cookie'] = cookie
  opts = {
    ...opts,
    formData: { _xsrf, name }
  }

  let { httpResponse, body } = await PostSync(opts)
  cookie = httpResponse.headers['set-cookie'].join('; ')

  return cookie
}

module.exports = createTask