'use strict'

function parseCookie(c) {
  let raw = c.split(';').map((pair) => pair.trim())
  let cookie = []
  for (let pair of raw) {
    let epos = pair.indexOf('=')
    pair = [ pair.substring(0, epos), pair.substring(epos + 1, pair.length) ]
    cookie[pair[0]] = pair[1]
  }
  return cookie
}

module.exports = parseCookie