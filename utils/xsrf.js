'use strict'

require('dotenv').config()

const parseCookie = require('./cookie')
const { GetSync } = require('../lib/RequestSync')

async function _xsrf(opts = {}) {
  const { httpResponse, body } = await GetSync(opts)
  let cookie = httpResponse.headers['set-cookie'].join('; ')
  return {
    cookie: cookie,
    _xsrf: _xsrfFromCookie(cookie)
  }
}

function _xsrfFromCookie(c) {
  return parseCookie(c)._xsrf
}

module.exports = {
  _xsrf,
  _xsrfFromCookie
}