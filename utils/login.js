'use strict'

require('dotenv').config()

const _XSRF = require('./xsrf')._xsrf
const { PostSync } = require('../lib/RequestSync')

/* 
  method POST
  /login
  _xsrf, username, password 
*/

async function Login() {
  let endpoint = process.env.AWS_URL + '/login'
  let opts = { url: endpoint }
  let { cookie, _xsrf } = await _XSRF(opts)

  opts = { 
    ...opts,
    headers: { 'cookie': cookie },
    formData: {
      _xsrf,
      username: process.env.username,
      password: process.env.password
    } 
  }

  const { httpResponse, body } = await PostSync(opts)
  cookie = httpResponse.headers['set-cookie'].join('; ')

  return cookie
}

module.exports = Login