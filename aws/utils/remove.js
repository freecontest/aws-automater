'use strict'

const exec = require('sync-exec')

function Remove(taskName) {
  console.log('  + Removing ' + taskName + ' from CMS')
  exec('yes | cmsRemoveTask ' + taskName)
  console.log('  + Removed successfully')
  return true 
}

module.exports = Remove