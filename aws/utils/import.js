'use strict'

const exec = require('sync-exec')

function Import(taskFolderPath, contestID) {
  console.log('  + Importing task to CMS')
  exec('cmsImportTask -c ' + contestID + ' ' + taskFolderPath)
  console.log('  + Imported successfully')
  return true 
}

module.exports = Import