'use strict'

const fs = require('fs')

function CleanUp(path) {
  if (fs.existsSync(path)) {
    fs.readdirSync(path).forEach(function(file, index){
      var curPath = path + "/" + file
      if (fs.lstatSync(curPath).isDirectory()) {
        CleanUp(curPath)
      } else {
        fs.unlinkSync(curPath)
      }
    })
    fs.rmdirSync(path);
  }
  return true
}

module.exports = CleanUp