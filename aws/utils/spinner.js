'use strict'

const Spinner = require('cli-spinner').Spinner

async function createSpinner(msg) {
  let spinner = new Spinner('%s ' + msg)
  spinner.setSpinnerString('|/-\\')
  spinner.start()
  return spinner
}

module.exports = createSpinner