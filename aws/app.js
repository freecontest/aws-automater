'use strict'

require('dotenv').config()

const _ = require('lodash')

const ask = require('readline-sync')
const { PrepareTask } = require('./task')
const CleanUp = require('./utils/cleanup')
const ImportToCMS = require('./utils/import')
const RemoveFromCMS = require('./utils/remove')
const createSpinner = require('./utils/spinner')

// AWS
const AWSLogin = require('../utils/login')
const AWSGetTasks = require('../fetch/tasks')

let Queue = { }
let Tasks = { }, Tests = { }

let config = { }
let existingTasks = { }

async function collectInformation() {
  config.taskFolderID = ask.question('DriveID of folder is containing tasks: ')
  config.testFolderID = ask.question('DriveID of folder is containing tests: ')
  config.contestID = ask.question('AWS contest ID (default = 1): ')
  config.contestID = ~~config.contestID
  if (isNaN(config.contestID) || config.contestID === 0) {
    config.contestID = 1
  }
  console.log(' ')
}

async function Init() {
  let spinner;
  
  // login into AWS
  spinner = await createSpinner('Logging into AWS')
  let session = await AWSLogin()
  spinner.stop(true)
  console.log('- Logged in AWS')

  // get AWS currently tasks
  spinner = await createSpinner('Get AWS currently exist tasks')
  existingTasks = await AWSGetTasks(session)
  existingTasks = existingTasks.map(t => t.name)
  spinner.stop(true)
  if (existingTasks.length > 0) {
    console.log('- Exitsting tasks: ' + existingTasks.join(', ').toString())
  }
  else {
    console.log('- There are no task is existing')
  }

  console.log('')

  return true 
}

async function preCreate() {
  let spinner;

  spinner = await createSpinner('Fetching tasks from folder ' + config.taskFolderID)
  let rawTasks = await require('../drive/tasks')(config.taskFolderID)
  spinner.stop(true)

  spinner = await createSpinner('Fetching tests from folder ' + config.testFolderID)
  let rawTests = await require('../drive/tasks')(config.testFolderID)
  spinner.stop(true)

  for (let task of rawTasks) {
    if (task.name.split('.')[1] === 'pdf') {
      Tasks[task.name.split('.')[0]] = task.id
    }
  }

  for (let test of rawTests) {
    Tests[test.name.split('.')[0]] = test.id
  }

  let officialTasks = _.intersection(
    rawTasks.map(e => e.name.split('.')[0]),
    rawTests.map(e => e.name.split('.')[0])
  )

  console.log('Found ' + officialTasks.length + ' tasks have both statement and dataset!')
  for (let _ = 0; _ < officialTasks.length; ++_) {
    console.log((_ + 1).toString() + '. ' + officialTasks[_])
    Queue[officialTasks[_]] = {
      statement: Tasks[officialTasks[_]],
      dataset: Tests[officialTasks[_]]
    }
  }

  let importRequest = ask.question('Input id of tasks split by "only" commas or "all" for all: ')
  if (importRequest !== 'all') {
    importRequest = importRequest.split(',').map(n => ~~n)
    for (let _ = 0; _ < officialTasks.length; ++_) {
      if (importRequest.indexOf(_ + 1) === -1) {
        delete Queue[officialTasks[_]]
      }
    }
  }
  return (Object.keys(Queue).length > 0)
}

async function ImportTasks() {
  console.log('')
  console.log('Import tasks is running...')
  console.log('All tasks will be attached to contest with id = ' + config.contestID)
  console.log('')
  for (let taskName in Queue) {
    // prepare task folder and importation
    let taskFolderPath = await PrepareTask({ name: taskName, ...Queue[taskName] })
    if (process.env.on === 'freecontest-vps') {
      if (existingTasks.includes(taskName)) {
        RemoveFromCMS(taskName)
      }
      ImportToCMS(taskFolderPath, config.contestID)
    }
  
    // clean up temp folder
    console.log('  + Cleaning up temporary task folder')
    CleanUp(taskFolderPath)
    console.log('  + Cleaned up successfully\n')
  }
}

;(async function() {
  await Init()
  await collectInformation()
  let continueToImport = await preCreate()
  if (continueToImport) {
    await ImportTasks()
  } else {
    console.log('Nothing to import!')
  }
})()