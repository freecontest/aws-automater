'use strict'

const fs = require('fs')
const path = require('path')
const extract = require('extract-zip')
const createSpinner = require('./utils/spinner')
const FileDownload = require('../drive/utils/download')

/*
  Structure of task folder
  @-outputonly
    |-input
    __|--input*.txt
    |-output
    __|--output*.txt
    |-statement
    __|--statement.pdf
    |-task.yaml
*/ 

function unZipSync(zipPath, destination) {
  return new Promise(function(resolve, reject) {
    extract(zipPath, {dir: destination}, function (err) {
      if (err) reject(err)
      resolve()
    })
  })
}

async function PrepareTask(task) {
  let spinner;

  // intro
  console.log('- ' + task.name)

  // create task folder
  const taskFolderPath = path.resolve(__dirname, './prepared_space/' + task.name)
  fs.mkdirSync(taskFolderPath)
  console.log('  + Created ' + task.name)

  // create statement folder
  const statementFolderPath = path.resolve(taskFolderPath, './statement')
  fs.mkdirSync(statementFolderPath)
  console.log('  + Created statement folder inside ' + task.name)

  // download statement
  spinner = await createSpinner('  + Downloading statement from drive')
  const statementPath = path.resolve(statementFolderPath, './statement.pdf')
  await FileDownload(task.statement, statementPath)
  spinner.stop(true)
  console.log('  + Downloaded statement')

  // download dataset
  spinner = await createSpinner('  + Downloading dataset from drive')
  const datasetPath = path.resolve(taskFolderPath, './dataset.zip')
  await FileDownload(task.dataset, datasetPath)
  spinner.stop(true)
  console.log('  + Downloaded dataset')

  // unzip dataset
  console.log('  + Unzipping dataset')
  const datasetTempPath = path.resolve(taskFolderPath, './dataset_temp')
  fs.mkdirSync(datasetTempPath)
  await unZipSync(datasetPath, datasetTempPath)
  console.log('  + Unzipped dataset successfully')

  let countTC = 0

  // refactor dataset
  console.log('  + Refactoring dataset')
  const inputPath = path.resolve(taskFolderPath, './input')
  const outputPath = path.resolve(taskFolderPath, './output')
  fs.mkdirSync(inputPath)
  fs.mkdirSync(outputPath)
  fs.readdirSync(datasetTempPath).forEach(function(fn) {
    ++countTC
    let type = fn.split('.')[0]
    let numb = parseInt(fn.split('.')[1]).toString()
    let currentPath = path.resolve(datasetTempPath, fn)
    let newPath = path.resolve(taskFolderPath, './' + type + '/' + type + numb + '.txt')
    fs.renameSync(currentPath, newPath)
  })
  fs.unlinkSync(datasetPath)
  fs.rmdirSync(datasetTempPath)
  console.log('  + Refactored successfully')

  // make task.yaml
  const TaskYAMLPath = path.resolve(taskFolderPath, './task.yaml')
  let pubTC = ''
  countTC /= 2;
  for (var i = 0; i < countTC; ++i) {
    pubTC += i.toString()
    if (i !== countTC - 1) {
      pubTC += ', '
    }
  }
  let content = ''
      content += 'name: "' + task.name + '"'
      content += '\n' + 'title: "' + task.name + '"'
      content += '\n' + 'score_mode: max'
      content += '\n' + 'time_limit: 1'
      content += '\n' + 'memory_limit: 1024'
      content += '\n' + 'n_input: ' + countTC.toString()
      content += '\n' + 'public_testcases: ' + pubTC
      content += '\n' + 'infile: ""'
      content += '\n' + 'outfile: ""'
      content += '\n' + 'total_value: 50.0'
      content += '\n' + 'primary_language: vi'
      content += '\n' + 'token_mode: disabled'
  fs.writeFileSync(TaskYAMLPath, content)
  console.log('  + Created task.yaml')

  return taskFolderPath
}

module.exports = { PrepareTask }